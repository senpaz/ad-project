import store from '../store'
export default function (to, from, nuxt) {
  if (store.getters.user) {
    nuxt()
  } else {
    nuxt('/login?logInErr=true')
  }
}
