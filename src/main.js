import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import BuyPopap from '@/components/Shared/Buy-popup'
import * as fb from 'firebase'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
Vue.component('app-buy-popup', BuyPopap)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    fb.initializeApp({
      apiKey: 'AIzaSyDTP2F2aOAP-5h1KfVdoezfIz6UB1xnEI8',
      authDomain: 'itc-ads-ffdd3.firebaseapp.com',
      projectId: 'itc-ads-ffdd3',
      storageBucket: 'itc-ads-ffdd3.appspot.com',
      databaseURL: 'https://itc-ads-ffdd3-default-rtdb.firebaseio.com',
      messagingSenderId: '520000573297',
      appId: '1:520000573297:web:9559c2044626e67474b0d0',
      measurementId: 'G-9BE3355P2C'
    })
    fb.auth().onAuthStateChanged(user => {
      this.$store.dispatch('autoLogginUser', user)
    })

    this.$store.dispatch('fetchAds')
  }
})
